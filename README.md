# top_gen_pass

## Build

To build projects you must have [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html).

Clone project and go to project folder. Next execute this command:
```shell
cargo build
```

And in `target/release` folder you can found `top_gen_pass` executable file.

## Modes

- Normal - ANSII chars.
- Extended - some additional utf-8 chars.
- Extreme - almost all utf-8 chars.

## License
MIT license. Details in LICENSE file.
