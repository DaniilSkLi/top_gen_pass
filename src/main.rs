use std::cmp;
use std::io;
use std::io::Write;
use rand::prelude::*;
use std::fs::File;

fn get_chars(mode: u8) -> Vec<char> {
    let mut chars: Vec<char> = Vec::new();

    for i in 0x21..0x7f {
        chars.push(char::from_u32(i).unwrap());
    }
    if mode >= 1 {
        for i in 0xa1..0x37f {
            chars.push(char::from_u32(i).unwrap());
        }
    }
    if mode >= 2 {
        for i in 0x37f..0xd7ff {
            chars.push(char::from_u32(i).unwrap());
        }
    }
    chars
}

fn gen_pass(length: u32, mode: u8) -> String {
    let chars: Vec<char> = get_chars(mode);
    let mut random = rand::thread_rng();
    let mut password = String::new();
    for _i in 0..length {
        let index: usize = (chars.len() as f64 * random.gen::<f64>()).floor() as usize;
        password.push(chars[index]);
    }
    password
}

fn get_num(placeholder: &str) -> u32 {
    print!("{}", placeholder);
    io::stdout().flush().unwrap();
    let mut number: String = String::new();
    io::stdin().read_line(&mut number).expect("Error line read");
    let number: u32 = number.trim().parse::<u32>().expect("Error number read");
    number
}

fn main() {
    let mode: u8 = get_num("Mode (0 - normal, 1 - extended, 2 - extreme): ") as u8;
    let mode: u8 = cmp::min(cmp::max(mode, 0), 2);
    let length: u32 = get_num("Length: ");

    let password: String = gen_pass(length, mode);
    if length > 200 {
        let mut file = File::create("./password.txt").unwrap();
        write!(file, "{}", password).unwrap();
        println!("Write to file \"password.txt\"");
    }
    else {
        println!("Password: {}", password);
    }
}
